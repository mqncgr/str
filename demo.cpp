#include <iostream>
#include <sstream>
#include <string>
#include "str.h"
#include <list>
#include <array>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <memory>

using str::print;

struct Rainbow {
	std::string text = "cucumber";
	bool boolean = true;
	int number = 123;
	int* pointer = &number;
	std::vector<std::vector<int>> nested = {
		{1, 2},
		{4, 5},
	};
	std::map<std::string, int> map = {
		{"1", 1}, {"2", 2}, {"3", 3}, {"4", 4} };

	STRINGIFY(Rainbow, text, boolean, number, pointer, nested, map)
};

int main() {

	std::cout << str::str() + "Hello" + "World" << "\n";

	bool myBool = true;
	print(myBool);

	int8_t myByte = -123;
	print(myByte);

	uint8_t myUnsignedByte = 234;
	print(myUnsignedByte);

	int myInt = 12345;
	print(myInt);

	const int myConst = 321;
	print(myConst);

	double myDouble = 123.4567;
	print(myDouble);

	print("Dennis");
    const char* myCString = "Ritchie";
    print(myCString);

	std::string myString = "All your base are belong to us!";
	print(myString);

	// rvalue
	print(6789);

	int& myReference = myInt;
	print(myReference);

	int* myRawPointer = &myInt;
	print(myRawPointer);

	int* myNullPtr = nullptr;
	print(myNullPtr);

	auto myUniquePointer = std::make_unique<int>(1337);
	print(myUniquePointer);

	std::unique_ptr<int> myUniqueNullPtr;
	print(myUniqueNullPtr);

	auto mySharedPointer = std::make_shared<int>(5417);
	print(mySharedPointer);

	std::weak_ptr myWeakPointer = mySharedPointer;
	print(myWeakPointer);

	std::array<std::string, 3> myArray{ "a", "b", "c" };
	print(myArray);

	std::list<int> myList{ 1, 2, 3 };
	print(myList);

	std::vector<double> myVector{ 1.0, 2.0, 3.0 };
	print(myVector);

	std::unordered_set<float> myUnorderedSet{ 7.0, 8.0, 9.0 };
	print(myUnorderedSet);

	std::map<std::string, int> myMap{ {"1", 1}, {"2", 2} };
	print(myMap);

	std::unordered_map<std::string, int> myUnorderedMap{
		{"1", 1}, {"2", 2}, {"3", 3}, {"4", 4} };
	print(myUnorderedMap);

	// doesn't work yet
	// auto myTuple = std::make_tuple("a", 2, 3.0);
	// print(myTuple);

	std::vector<std::vector<std::vector<std::vector<int>>>> myNesting{ {{{0}}} };
	for (int8_t maxDepth = 0; maxDepth <= 4; maxDepth++) {
		for (int8_t inlineAt = 0; inlineAt <= maxDepth; inlineAt++) {
			std::cout << "maxDepth: " << int(maxDepth)
				<< ", inlineAt: " << int(inlineAt);
			print(myNesting, str::Fmt{maxDepth, inlineAt});
		}
	}

	Rainbow rainbow;
	for (int8_t maxDepth = 0; maxDepth <= 4; maxDepth++) {
		print(rainbow, str::Fmt{maxDepth});
	}
	str::Fmt fmt;
	fmt.colored=false;
	print(rainbow, fmt);

	return 0;
}