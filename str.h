
#pragma once

#include <deque>
#include <fstream>
#include <memory>
#include <ostream>
#include <string>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>


// macro that can be used to stringify class members and values
#define STRINGIFY(name, ...) \
	std::string str(const str::Fmt& fmt) const{ \
		std::string membersStr = str::replace(#__VA_ARGS__, ",", ""); \
		std::stringstream members(membersStr); \
		auto [values, fmt_] = str::strVA(__VA_ARGS__, fmt); \
		std::vector<str::Protected> list; \
		for(const auto &val:values){ \
			std::string member; \
			members >> member; \
			list.push_back({ \
				str::color(member, str::BRIGHT_GREEN, fmt) \
				+ " = " + val.s}); \
		} \
		if(fmt.depth < fmt.maxDepth){ \
			return str::str() \
				+ str::color(#name, str::BRIGHT_BLUE, fmt) \
				+ "(" + str::strIter(list, fmt) + ")"; \
		} \
		else{ \
			return str::str() \
			+ str::color(#name, str::BRIGHT_BLUE, fmt) \
			+ "(" + values[0].s + ")"; \
		} \
	}

namespace str {

	// ansi escape color codes
	const char* RESET = "\033[0m";
	const char* BLACK = "\033[30m";
	const char* RED = "\033[31m";
	const char* GREEN = "\033[32m";
	const char* YELLOW = "\033[33m";
	const char* BLUE = "\033[34m";
	const char* MAGENTA = "\033[35m";
	const char* CYAN = "\033[36m";
	const char* WHITE = "\033[37m";
	const char* GRAY = "\033[90m";
	const char* BRIGHT_BLACK = "\033[90m";
	const char* BRIGHT_RED = "\033[91m";
	const char* BRIGHT_GREEN = "\033[92m";
	const char* BRIGHT_YELLOW = "\033[93m";
	const char* BRIGHT_BLUE = "\033[94m";
	const char* BRIGHT_MAGENTA = "\033[95m";
	const char* BRIGHT_CYAN = "\033[96m";
	const char* BRIGHT_WHITE = "\033[97m";

	// formatting state holder
	struct Fmt {
		char maxDepth = 2;
		char inlineAt = maxDepth - 1;
		bool colored = true;

		char depth = 0;
		//std::string color = RESET;
	};

	// increase nesting depth
	Fmt operator++(Fmt fmt, int) {
		fmt.depth++;
		return fmt;
	}

	// string that should not be formatted further
	struct Protected { std::string s = ""; };

	// coloring
	inline std::string colorBegin(std::string col, const Fmt& fmt) {
		return fmt.colored? col : "";
	}

	inline std::string colorEnd(const Fmt& fmt) {
		return fmt.colored? RESET : "";
	}

	template <typename T>
	inline std::string color(T s, std::string col, const Fmt& fmt) {
		std::stringstream ss;
		ss  << colorBegin(col, fmt) << s << colorEnd(fmt);
		return ss.str();
	}

	// pad line with indentation
	inline std::string indent(char depth) {
		std::stringstream ss;
		for (size_t i = 0; i < depth; i++) {
			ss << "    ";
		}
		return ss.str();
	}

	// SFINAE magic
	template <typename T, typename = void>
	struct is_iterable : std::false_type {};
	template <typename T>
	struct is_iterable<T, std::void_t<
		decltype(std::declval<T>().begin()),
		decltype(std::declval<T>().end())>
	> : std::true_type {};

	template <typename T, typename = void>
	struct is_kv_iterable : std::false_type {};
	template <typename T>
	struct is_kv_iterable<T, std::void_t<
		decltype(std::declval<T>().begin()->first),
		decltype(std::declval<T>().begin()->second),
		decltype(std::declval<T>().end()->first),
		decltype(std::declval<T>().end()->second)>
	> : std::true_type {};

	template <typename T, typename = void>
	struct has_str_member : std::false_type {};
	template <typename T>
	struct has_str_member<T, std::void_t<
		decltype(std::declval<T>().str())
	> > : std::true_type {};

	template <typename T, typename = void>
	struct has_str_member_with_fmt : std::false_type {};
	template <typename T>
	struct has_str_member_with_fmt<T, std::void_t<
		decltype(std::declval<T>().str(Fmt()))
	> > : std::true_type {};

	template <typename T, typename = void>
	struct has_deref : std::false_type {};
	template <typename T>
	struct has_deref<T, std::void_t<
		decltype(std::declval<T>().operator*())
	> > : std::true_type {};

	// helper function so you can write
	// str() + "myString: " + myString
	inline std::string str() {
		return "";
	}

	// booleans
	// c++ wants to cast a lot into bools which we have to prevent
	template <typename T, std::enable_if_t<
		std::is_same<T, bool>::value, int
	> = 0>
		inline std::string str(T val, const Fmt& fmt = {}) {
		return color(val ? "true" : "false", BRIGHT_MAGENTA, fmt);
	}

	// numbers
	template <typename T, std::enable_if_t<
		std::is_arithmetic<T>::value
		&& !std::is_same<T, bool>::value, int
	> = 0>
		inline std::string str(T val, const Fmt& fmt = {}) {
		return color(std::to_string(val), BRIGHT_RED, fmt);
	}

	// strings
	inline std::string str(const std::string& s, const Fmt& fmt = {}) {
		std::stringstream ss;
		ss << '"';
		if (fmt.depth >= fmt.maxDepth && s.length() > 8) {
			ss << s.substr(0, 8) + "...";
		}
		else {
			ss << s;
		}
		ss << '"';
		return color(ss.str(), YELLOW, fmt);
	}

	// c strings
	inline std::string str(const char* s, const Fmt& fmt = {}) {
		return str(std::string(s));
	}

	// protected strings
	inline std::string str(const Protected& s, const Fmt& fmt = {}) {
		return s.s;
	}

	// objects with .str() member (like stringstream)
	template <typename T, std::enable_if_t<
		has_str_member<T>::value, int
	> = 0>
		inline std::string str(const T& s, const Fmt & = {}) {
		return s.str();
	}

	// objects with .str(fmt) member (for user-defined classes)
	template <typename T, std::enable_if_t<
		has_str_member_with_fmt<T>::value, int
	> = 0>
		inline std::string str(const T& s, const Fmt& fmt = {}) {
		return s.str(fmt);
	}

	// raw pointers
	template <typename T, std::enable_if_t<
		std::is_pointer<T>::value, int
	> = 0>
		inline std::string str(const T& p, const Fmt& fmt = {}) {
		std::stringstream ss;
		if (p) {
			ss << color(p, BRIGHT_CYAN, fmt);
			if (fmt.depth < fmt.maxDepth) {
				ss << "->" << str(*p, fmt++);
			}
		}
		else {
			ss << color("0x0", BRIGHT_CYAN, fmt);
		}
		return ss.str();
	}

	// smart pointers (or anything that has operator*)
	template <typename T, std::enable_if_t<
		has_deref<T>::value, int
	> = 0>
		inline std::string str(const T& p, const Fmt& fmt = {}) {
			std::stringstream ss;
		if (&p.operator*()) {
			ss << color(&p.operator*(), BRIGHT_CYAN, fmt);
			if (fmt.depth < fmt.maxDepth) {
				ss << "->" << str(p.operator*(), fmt++);
			}
		}
		else {
			ss << "0x0";
		}
		return color(ss.str(), BRIGHT_CYAN, fmt);
	}

	// weak pointers
	template <typename T>
	inline std::string str(const std::weak_ptr<T>& p, const Fmt& fmt = {}) {
		return str(p.lock());
	}

	// std::pair which is used for key-value pairs
	template <typename K, typename V>
	inline std::string str(const std::pair<K, V>& keyval, const Fmt& fmt = {}) {
		return str(keyval.first, fmt) + ": " + str(keyval.second, fmt);
	}

	// helper for iterables
	template <typename T>
	inline std::string strIter(const T& iterable, const Fmt& fmt = {}) {
		if (iterable.size() == 0) { return ""; }
		if (fmt.depth >= fmt.maxDepth) { return "..."; }
		std::stringstream ss;
		if (fmt.depth < fmt.inlineAt) { ss << "\n" << indent(fmt.depth + 1); }
		bool first = true;
		for (auto& i : iterable) {
			if (!first) {
				if (fmt.depth < fmt.inlineAt) { ss << ",\n" << indent(fmt.depth + 1); }
				else { ss << ", "; }
			}
			ss << str(i, fmt++);
			first = false;
		}
		if (fmt.depth < fmt.inlineAt) { ss << "\n" << indent(fmt.depth); }
		return ss.str();
	}

	// array-like iterables
	template <typename T, std::enable_if_t<
		is_iterable<T>::value && !is_kv_iterable<T>::value, int
	> = 0>
		inline std::string str(const T& arr, const Fmt& fmt = {}) {
		return std::string("[") + strIter(arr, fmt) + "]";
	}

	// map-like iterables
	template <typename T, std::enable_if_t<
		is_kv_iterable<T>::value, int
	> = 0>
		inline std::string str(const T& map, const Fmt& fmt = {}) {
		return std::string("{") + strIter(map, fmt) + "}";
	}

	// helpers for calls with variadic arguments
	inline std::pair<std::deque<Protected>, Fmt>
		strVA(const Fmt& fmt = {}) {
		return { {}, fmt };
	}

	template <typename T, typename ... Ts>
	inline std::pair<std::deque<Protected>, Fmt>
		strVA(const T& head, const Ts&... tail) {
		auto [queue, fmt] = strVA(tail...);
		queue.push_front({ str(head, fmt++) });
		return { queue, fmt };
	}

	// calls with variadic arguments
	// there must be at least two arguments
	// and the second one must not be a formatter
	template <typename T1, typename T2, std::enable_if_t<
		!std::is_same<T2, Fmt>::value, int
	> = 0>
		inline std::string str(const T1& arg1, const T2& arg2) {
		auto [queue, fmt] = strVA(arg1, arg2);
		return str("(") + strIter(queue, fmt) + ")";
	}

	template <typename T1, typename T2, typename ... Ts>
	inline std::string str(const T1& arg1, const T2& arg2, const Ts&... args) {
		auto [queue, fmt] = strVA(arg1, arg2, args...);
		return str("(") + strIter(queue, fmt) + ")";
	}

	// tuples
	//template <typename ... Ts>
	//inline std::string str(const std::tuple<Ts...> t, const Fmt& fmt = {}) {
	//	return std::apply(str, std::tuple_cat(t, std::make_tuple(fmt)));
	//}

	// convenience function for printing
	template <typename ... Ts>
	inline void print(const Ts&... args) {
		std::cout << str(args...) << "\n";
	}

	// load string from file
	inline std::string load(const std::string& filename, bool binary = false) {
		// https://stackoverflow.com/a/2912614
		std::ifstream ifs(
			filename,
			binary ?
			std::ios::in | std::ios::binary
			: std::ios::in
		);
		if (!ifs) {
			throw std::runtime_error(filename + " not found");
		}
		return std::string(
			(std::istreambuf_iterator<char>(ifs)),
			(std::istreambuf_iterator<char>()));
	}

	// save string to file
	inline void save(const std::string& filename, const std::string& content) {
		std::ofstream ofs(filename);
		if (!ofs) {
			throw std::runtime_error(filename + " could not be written");
		}
		ofs << content;
	}

	// various string replace functions
	inline void replaceInplace(std::string& str, const std::string& search, const std::string& repl) {
		// https://stackoverflow.com/a/24315631
		size_t start_pos = 0;
		while ((start_pos = str.find(search, start_pos)) != std::string::npos) {
			str.replace(start_pos, search.length(), repl);
			start_pos += repl.length(); // Handles case where 'repl' is a substring of 'search'
		}
	}

	inline std::string replace(std::string str, const std::string& search, const std::string& repl) {
		replaceInplace(str, search, repl);
		return str;
	}

	inline void replaceInplace(std::string& str,
		const std::vector<std::pair<std::string, std::string>>& subs) {
		for (const auto& s : subs) {
			replaceInplace(str, s.first, s.second);
		}
	}

	inline std::string replace(std::string str, const std::vector<std::pair<std::string, std::string>>& subs) {
		replaceInplace(str, subs);
		return str;
	}

}